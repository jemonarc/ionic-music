import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.example.app',
  appName: 'platzi-music',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
