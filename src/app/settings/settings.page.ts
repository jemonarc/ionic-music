import { Component, OnInit } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { Camera, CameraResultType, CameraSource} from '@capacitor/camera';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit {
  userImage = "assets/img/logo.png";
  photo: SafeResourceUrl; //type for secure url
  constructor(private sanitize: DomSanitizer) { }

  ngOnInit() {
  }

  async takePhoto(){
    const image = await Camera.getPhoto({
      resultType: CameraResultType.DataUrl,
      source: CameraSource.Camera,
      quality: 100,
      allowEditing: false
    });
    this.photo = this.sanitize.bypassSecurityTrustResourceUrl(image && image.dataUrl)
    console.log(image);
  }

}
