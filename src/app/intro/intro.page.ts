import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-intro',
  templateUrl: './intro.page.html',
  styleUrls: ['./intro.page.scss'],
})
export class IntroPage {

  slideOpts = {
    initialSllide: 0,
    slidePreView: 1,
    centeredSlides: true,
    spped: 400
  };

  slides = [
    {title:'Escucha tu música', subtitle: 'favorita', description: 'Andas en mi cabeza nena a todas horas', icon: 'play'},
    {title:'Escucha tu música', subtitle: 'favorita', description: 'el mundo me da vueltas tu me descontrolas', icon: 'planet-outline'},
    {title:'Escucha tu música', subtitle: 'favorita', description: 'por ti me la paso imaginando que ...', icon: 'cloud-outline'},
    {title:'Escucha tu música', subtitle: 'favorita', description: 'contigo me casé y por siempre te amé', icon: 'heart-outline'}
  ]

  constructor(private router: Router, private storage: Storage) { }

  finish() {
    this.storage.create();
    this.storage.set("isIntroShowed", true);
    this.router.navigateByUrl('/login');
  }

}
