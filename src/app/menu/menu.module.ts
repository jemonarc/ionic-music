import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { MenuPageRoutingModule } from './menu-routing.module';

import { MenuPage } from './menu.page';

const routes: Routes = [{
  path: "",
  component: MenuPage,
  children: [
    {
      path: 'home',
      loadChildren: () => import('../home/home.module').then( m => m.HomePageModule)
    },
      {
      path: 'settings',
      loadChildren: () => import('../settings/settings.module').then( m => m.SettingsPageModule)
    },
    {
      path:"", redirectTo: "home", pathMatch: "full"
    },
    {
      path: 'sports',
      loadChildren: () => import('../sports/sports.module').then( m => m.SportsPageModule)
    },
  ]
}];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MenuPageRoutingModule,
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule],
  declarations: [MenuPage]
})
export class MenuPageModule {}
